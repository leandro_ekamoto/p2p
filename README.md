# P2P

Comandos para compilar/executar: 
  - cliente_servidor.c gcc -o cliente_servidor cliente_servidor.c -lpthread 
  - sendBroadcast.c gcc -o send sendBroadcast.c
  
Passos para execução: 
  - execute o cliente_servidor com o comando, se possível execute em duas ou mais máquinas na mesma rede: ./cliente_servidor 
  - execute o send com o comando: ./send 192.168.0.255 mensagemteste
  - depois que executar o send o cliente_servidor irá receber uma mensagem

Observações importantes:
  - O firewall pode bloquear o broadcast
  -  Verificar o endereço de broadcast e a interface utilizando o comando ifconfig

