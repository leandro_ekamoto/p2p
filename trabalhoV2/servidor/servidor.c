#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <netinet/in.h>

#define BUFFSIZEDIR 4000
#define BUFFSIZE 800
#define MAXPENDING 5
#define BUFFSIZEFILE 512
#define PORTAUDP 8088
#define SA struct sockaddr
#define SI struct sockaddr_in
#define RAIZ "."

/* Informações passada ao cliente sobre o arquivo */
struct CabecaoArquivo {
    char arquivo[250];
    long size;
    unsigned sum;
    int partes;
};

/* Para armazenar informações sobre o cliente */
struct InformacaoConexao {
    int connfd;
    char ip[14];
};

/* Thread para tratamento para várias conexões */
static void *mensagem_thread(void *arg);
/* escuta */
int recebeMensagem(int sockfd, char *b);
/* envio de arquivos via TCP */
void enviarArquivoTcp(int connfd, char *c);
/* Cria socket udp para envio */
int CriarSocketUdpClient(char *ip, SI *si_other);
/* Envio de arquivos UDP, somente é enviado o cabeçalho de informar do arquivo na forma TCP */
void enviarArquivoUdp(int connfd, char *c, char *ip);
/* retorna o tamanho do arquivos em byte*/
long tamanhoArquivoByte(char *caminho);
/* forma de validar um arquivo depois de transferido */
unsigned checksum(char *c);
/* lista tanto arquivos com diretorios também */
void listaDiretorio(char *diretorio, int espaco, char *listDiretorio);

/* O arquivo é dividido em várias partes com um tamanho de 512 bytes e enviado de duas formas TCP e UDP.
 * O servidor também recebe arquivos, porém somente em TCP que é uma forma garantida de recebimento.
 */

int main(int argc, char *argv[]) {
    struct InformacaoConexao *inf;
    int listenfd;
    socklen_t clientlen;
    pthread_t tid;
    struct sockaddr_in servaddr, client;

    if (argc != 2){
        printf("Use: ./server <porta>\n");
        return 0;
    }

    if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Falha ao criar o socket.\n");
        return 0;
    }

    memset((char *) &servaddr, 0, sizeof(servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(atoi(argv[1]));

    if (bind(listenfd, (SA *) & servaddr, sizeof (servaddr)) < 0) {
        printf("Falha ao observar o socket do servidor.\n");
        return 0;
    }

    if (listen(listenfd, MAXPENDING) < 0) {
        printf("Falha ao tentar escutar o socket do servidor.\n");
        return 0;
    }

    for (;;) {
        clientlen = sizeof (client);
        inf = (struct InformacaoConexao *) malloc(sizeof (struct InformacaoConexao));
        inf->ip[0] = '\0';

        inf->connfd = accept(listenfd, (SA *) &client, &clientlen);
        printf("-------- Conexão estabelecida com %s --------\n", inet_ntoa(client.sin_addr));
        strcat(inf->ip, inet_ntoa(client.sin_addr));
        pthread_create(&tid, NULL, &mensagem_thread, inf);
    }

    close(listenfd);
    exit(0);
}

static void *mensagem_thread(void *arg) {
    int connfd, n;
    struct InformacaoConexao *inf;
    char buffer[BUFFSIZE];
    char *menu = "Escolha a opção desejada:\n1)Listar arquivos\n2)Baixar Arquivo via TCP\n3)Baixar Arquivo via UDP\n4)Enviar aquivo\n";
    int exit = 0;

    inf = ((struct InformacaoConexao *) arg);
    connfd = inf->connfd;
    pthread_detach(pthread_self());

    while (!exit) {
        if (recebeMensagem(connfd, buffer) <= 0) {
            exit = 1;
            break;
        }

        if (strcmp(buffer, "menu") == 0) {
            send(connfd, menu, strlen(menu), 0);

            if (recebeMensagem(connfd, buffer) <= 0) {
                exit = 1;
                break;
            }
        }

        if (strcmp(buffer, "1") == 0) {
            char diretorio[BUFFSIZEFILE];
            listaDiretorio(RAIZ, 0, diretorio);
            send(connfd, diretorio, BUFFSIZEFILE, 0);
            buffer[0] = '\0';
        } else if (strcmp(buffer, "2") == 0) {
            send(connfd, "\nInforme o arquivo\n", strlen("\nInforme o arquivo\n"), 0);
            if ((n = recv(connfd, buffer, BUFFSIZE, 0)) <= 0) {
                exit = 1;
                break;
            }

            enviarArquivoTcp(connfd, buffer);
            buffer[0] = '\0';
        } else if (strcmp(buffer, "3") == 0) {
            send(connfd, "\nInforme o arquivo\n", strlen("\nInforme o arquivo\n"), 0);
            if ((n = recv(connfd, buffer, BUFFSIZE, 0)) <= 0) {
                exit = 1;
                break;
            }

            enviarArquivoUdp(connfd, buffer, inf->ip);
            buffer[0] = '\0';
        } else if (strcmp(buffer, "4") == 0) {
            struct CabecaoArquivo cArquivo;
            if ((n = recv(connfd, &cArquivo, sizeof (struct CabecaoArquivo), 0)) <= 0) {
                exit = 1;
                break;
            } else {
                char bufferA[BUFFSIZEFILE];
                if (strcmp(cArquivo.arquivo, "erro") != 0) {
                    int j = 0;
                    FILE *fn = fopen(cArquivo.arquivo, "wb");
                    while (cArquivo.partes != j) {
                        j++;
                        if ((n = recv(connfd, bufferA, BUFFSIZEFILE, 0)) <= 0) {
                            exit = 1;
                            break;
                        }

                        if (cArquivo.partes != j || cArquivo.size % BUFFSIZEFILE == 0) {
                            fwrite(bufferA, 1, BUFFSIZEFILE, fn);
                        } else {
                            fwrite(bufferA, 1, (cArquivo.size % BUFFSIZEFILE), fn);
                        }
                    }
                    fclose(fn);
                    if (cArquivo.sum == checksum(cArquivo.arquivo)) {
                        send(connfd, "Recebido com sucesso.", strlen("Recebido com sucesso."), 0);
                    } else {
                        send(connfd, "Não foi possível receber o arquivos.", strlen("Não foi possível receber o arquivos."), 0);
                    }
                }
            }
        }
    }

    close(connfd);
    free(inf);
    return NULL;
}

int recebeMensagem(int sockfd, char *b) {
    int n;
    if ((n = recv(sockfd, b, BUFFSIZE, 0)) <= 0) {
        return n;
    }

    b[n] = '\0';
    return n;
}

void enviarArquivoTcp(int connfd, char *c) {
    FILE *f;
    struct CabecaoArquivo cArquivo;

    char buffer[BUFFSIZEFILE];
    long size;
    int check, qtdParteInteira = 0, n, i;

    size = tamanhoArquivoByte(c);
    check = checksum(c);

    if (NULL == (f = fopen(c, "rb")) || size <= 0 || check <= 0) {
        cArquivo.arquivo[0] = '\0';
        strcat(cArquivo.arquivo, "erro");
        send(connfd, &cArquivo, sizeof (struct CabecaoArquivo), 0);
        return;
    }

    qtdParteInteira = size / BUFFSIZEFILE;
    if (size % BUFFSIZEFILE != 0) {
        qtdParteInteira++;
    }

    cArquivo.arquivo[0] = '\0';
    strcat(cArquivo.arquivo, c);
    cArquivo.size = size;
    cArquivo.sum = check;
    cArquivo.partes = qtdParteInteira;

    send(connfd, &cArquivo, sizeof (struct CabecaoArquivo), 0);

    memset(buffer, 0, sizeof(buffer));

    for (i = 0; i < qtdParteInteira; i++) {
        n = fread(buffer, 1, BUFFSIZEFILE, f);
        send(connfd, buffer, n, 0);
    }

    fclose(f);
}

/* somente cabeçalho do arquivo é enviado TCP, porém as partes do arquivo são enviadas via UDP*/
void enviarArquivoUdp(int connfd, char *c, char *ip) {
    FILE *f;
    SI si_other;
    struct CabecaoArquivo cArquivo;
    int sockeUdp;

    char buffer[BUFFSIZEFILE];
    long size;
    int check, qtdParteInteira = 0, n, i;

    size = tamanhoArquivoByte(c);
    check = checksum(c);

    if (NULL == (f = fopen(c, "rb")) || size <= 0 || check <= 0) {
        cArquivo.arquivo[0] = '\0';
        strcat(cArquivo.arquivo, "erro");
        send(connfd, &cArquivo, sizeof (struct CabecaoArquivo), 0);
        return;
    }

    qtdParteInteira = size / BUFFSIZEFILE;
    if (size % BUFFSIZEFILE != 0) {
        qtdParteInteira++;
    }

    cArquivo.arquivo[0] = '\0';
    strcat(cArquivo.arquivo, c);
    cArquivo.size = size;
    cArquivo.sum = check;
    cArquivo.partes = qtdParteInteira;

    send(connfd, &cArquivo, sizeof (struct CabecaoArquivo), 0);

    memset(buffer, 0, sizeof(buffer));
    
    sockeUdp = CriarSocketUdpClient(ip, &si_other);
    if(sockeUdp < 0){
        fclose(f);
        return;
    }
    
    for (i = 0; i < qtdParteInteira; i++) {
        n = fread(buffer, 1, BUFFSIZEFILE, f);
        sendto(sockeUdp, buffer, n, 0 , (struct sockaddr *) &si_other, sizeof(si_other));
    }
    
    close(sockeUdp);
    fclose(f);
}

int CriarSocketUdpClient(char *ip, SI *si_other) {
    int sockUdp;

    if ((sockUdp = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        return -1;
    }

    memset((char *) si_other, 0, sizeof (*si_other));
    
    si_other->sin_family = AF_INET;
    si_other->sin_port = htons(PORTAUDP);
    
    if (inet_aton(ip, &si_other->sin_addr) == 0) {
        return -1;
    }
    
    return sockUdp;
}

long tamanhoArquivoByte(char *caminho) {
    FILE *file;
    long size;
    file = fopen(caminho, "rb");
    if (file) {
        fseek(file, 0, SEEK_END);
        size = ftell(file);
        rewind(file);
        fclose(file);
        return size;
    } else {
        return 0;
    }
}

unsigned checksum(char *c) {
    FILE *f;
    unsigned char *buf, *bufI;
    unsigned int seed = 0;
    long size;
    size_t i, result;

    if (NULL == (f = fopen(c, "rb"))) {
        return 0;
    }

    size = tamanhoArquivoByte(c);
    buf = (unsigned char *) malloc(sizeof(unsigned char) * size);
    bufI = buf;

    if (buf == NULL) {
        return 0;
    }

    result = fread(buf, 1, size, f);
    if (result != size) {
        fclose(f);
        free(buf);
        return 0;
    }

    for (i = 0; i < result; ++i) {
        seed += (unsigned int) (*buf++);
    }

    fclose(f);
    free(bufI);
    return seed;
}

void listaDiretorio(char *diretorio, int espaco, char *listDiretorio) {
    char aux[256];
    DIR *open;
    struct dirent **namelist;
    int n, i;


    n = scandir(diretorio, &namelist, 0, alphasort);
    listDiretorio[0] = '\0';
    while (n--) {
        if ((strcmp(namelist[n]->d_name, ".") != 0) && (strcmp(namelist[n]->d_name, "..") != 0)) {
            strcpy(aux, diretorio);
            strcat(aux, "/");
            strcat(aux, namelist[n]->d_name);
            if ((open = opendir(aux))) {
                for (i = 0; i < espaco; i++) {
                    strcat(listDiretorio, " ");
                }
                strcat(listDiretorio, aux);
                strcat(listDiretorio, "\n");
                listaDiretorio(aux, espaco + 4, listDiretorio);
                closedir(open);
            } else {
                for (i = 0; i < espaco; i++) {
                    strcat(listDiretorio, " ");
                }
                strcat(listDiretorio, aux);
                strcat(listDiretorio, "\n");
            }
        }
        free(namelist[n]);
    }
    free(namelist);

    return;
}