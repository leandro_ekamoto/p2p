#include <stdio.h>    
#include <stdlib.h>         
#include <string.h>        
#include <sys/socket.h>     
#include <netinet/in.h>     
#include <arpa/inet.h>     
#include <unistd.h>    
#include <sys/time.h>
#include <time.h>

#define BUFFSIZEDIR 4000
#define BUFFSIZE 800
#define BUFFSIZEFILE 512
#define PORTAUDP 8088
#define SA struct sockaddr
#define SI struct sockaddr_in

struct CabecaoArquivo {
    char arquivo[250];
    long size;
    unsigned sum;
    int partes;
};

/* Criação de um socket UDP para recebimento de arquivos do servidor */
int criarSocketUdp();
/* escuta */
int recebeMensagem(int sockfd, char *b, int tamanho);
/* cria socket para envio troca de informações com o servidor */
int criarSocketUdp();
/* Retorna o tamanho em byte do arquivo */
long tamanhoArquivoByte(char *caminho);
/* Forma de garantir que o arquivo baixo é exatamente o clone do servidor */
unsigned checksum(char *c);
/* Envia arquivo para o servidor */
int enviarArquivoTcp(int connfd, char *c);

/* 
 * Para recebimento UDP foi necessário fixar uma porta 8088, pois o cliente se torna servidor e o servidor cliente na passagem dos arquivos 
 */

int main(int argc, char *argv[]) {
    FILE *fn;
    int sockfd, n, sair = 0, menu = 1, j;
    struct CabecaoArquivo cArquivo;
    struct sockaddr_in servaddr;
    char buffer[BUFFSIZE], bufferA[BUFFSIZEFILE], lixo;

    if (argc != 3){
        printf("Use: ./cliente <server_ip> <porta>\n");
        return 0;
    }

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        printf("ERROR opening socket\n");
        return 0;
    }

    memset((char *) &servaddr, 0, sizeof(servaddr));
    
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(argv[1]);
    servaddr.sin_port = htons(atoi(argv[2]));

    if (connect(sockfd, (SA *) & servaddr, sizeof (servaddr)) < 0) {
        printf("Falha ao conectar!\n");
        return 0;
    }

    while (!sair) {

        if (menu) {
            send(sockfd, "menu", strlen("menu"), 0);

            recebeMensagem(sockfd, buffer, BUFFSIZE);
            printf("%s", buffer);

            printf("digite Opcão: ");
            scanf("%[^\n]", buffer);
            scanf("%c", &lixo);
            send(sockfd, buffer, strlen(buffer), 0);
            menu = 0;
        }

        if (strcmp(buffer, "1") == 0) {
            char diretorio[BUFFSIZEDIR];
            recebeMensagem(sockfd, diretorio, BUFFSIZEDIR);
            printf("\n%s\n", diretorio);
            menu = 1;
        } else if (strcmp(buffer, "2") == 0) {
            recebeMensagem(sockfd, buffer, BUFFSIZE);
            printf("%s", buffer);

            scanf("%[^\n]", buffer);
            scanf("%c", &lixo);

            send(sockfd, buffer, strlen(buffer), 0);

            if ((n = recv(sockfd, &cArquivo, sizeof (struct CabecaoArquivo), 0)) <= 0) {
                printf("Problemas com a conexão\n");
                exit(0);
            }

            if (strcmp(cArquivo.arquivo, "erro") == 0) {
                printf("Problemas ao tentar fazer download do arquivo.\n");
            } else {
                int j = 0;
                FILE *fn = fopen(cArquivo.arquivo, "wb");
                printf("Efetuando Download ...\n");
                while (cArquivo.partes != j) {
                    j++;
                    if ((n = recv(sockfd, bufferA, BUFFSIZEFILE, 0)) <= 0) {
                        printf("Problemas com a conexão\n");
                        exit(0);
                    }

                    if (cArquivo.partes != j || cArquivo.size % BUFFSIZEFILE == 0) {
                        fwrite(bufferA, 1, BUFFSIZEFILE, fn);
                    } else {
                        fwrite(bufferA, 1, (cArquivo.size % BUFFSIZEFILE), fn);
                    }
                }
                fclose(fn);
                printf("\nChecksum Original: %x\nChecksum Baixado: %x\n", cArquivo.sum, checksum(cArquivo.arquivo));
                if (cArquivo.sum == checksum(cArquivo.arquivo)) {
                    printf("Download feito com sucesso.\n\n");
                } else {
                    printf("Falha ao efetuar Download.\n\n");
                }
            }
            menu = 1;
        } else if (strcmp(buffer, "3") == 0) {
            recebeMensagem(sockfd, buffer, BUFFSIZE);
            printf("%s", buffer);

            scanf("%[^\n]", buffer);
            scanf("%c", &lixo);

            send(sockfd, buffer, strlen(buffer), 0);

            if ((n = recv(sockfd, &cArquivo, sizeof (struct CabecaoArquivo), 0)) <= 0) {
                printf("Problemas com a conexão\n");
                exit(0);
            }

            if (strcmp(cArquivo.arquivo, "erro") == 0) {
                printf("Problemas ao tentar fazer download do arquivo.\n");
            } else {
                int sockUdp = criarSocketUdp();
                int sair = 0;
                
                if(sockUdp <= 0){
                    printf("Problemas na criação do socket UDP\n");
                    menu = 1;
                    break;
                }
                
                fn = fopen(cArquivo.arquivo, "wb");
                j = 0;
    
                printf("Efetuando Download ...\n");
                while(!sair && j != cArquivo.partes){
                    j++;
                    if ((n = recv(sockUdp, bufferA, BUFFSIZEFILE, 0)) <= 0) {
                        printf("Problemas ao fazer download udp\n");
                        sair = 1;
                        break;
                    }

                    if (cArquivo.partes != j || cArquivo.size % BUFFSIZEFILE == 0) {
                        fwrite(bufferA, 1, BUFFSIZEFILE, fn);
                    } else {
                        fwrite(bufferA, 1, (cArquivo.size % BUFFSIZEFILE), fn);
                    }
                }
                
                fclose(fn);
                printf("\nChecksum Original: %x\nChecksum Baixado: %x\n", cArquivo.sum, checksum(cArquivo.arquivo));
                if (cArquivo.sum == checksum(cArquivo.arquivo)) {
                    printf("Download feito com sucesso.\n\n");
                } else {
                    printf("Falha ao efetuar Download.\n\n");
                }
                
                close(sockUdp);
            }
            menu = 1;
        } else if (strcmp(buffer, "4") == 0) {
            printf("Informe o arquivo para envio: ");
            scanf("%[^\n]", buffer);
            scanf("%c", &lixo);

            if (!enviarArquivoTcp(sockfd, buffer)) {
                recebeMensagem(sockfd, buffer, BUFFSIZE);
                printf("\n%s\n\n", buffer);
            }

            menu = 1;
        }
    }

    printf("\n");
    close(sockfd);
    exit(0);
}

int criarSocketUdp() {
    SI servAddr;
    int sockUdp;
    struct timeval tv;
    
    tv.tv_sec = 0;
    tv.tv_usec = 500000;
    
    if ((sockUdp = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        return -1;
    }
    
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(PORTAUDP);
    
    if((bind (sockUdp, (SA *) &servAddr,sizeof(servAddr))) < 0){
        return -1;
    }
    
    /* Setando timeout do socket para aguardar até 5ms a chegada de pacotes */
    if (setsockopt(sockUdp, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        return -1;
    }
    
    return sockUdp;
}

int recebeMensagem(int sockfd, char *b, int tamanho) {
    int n;
    if ((n = recv(sockfd, b, tamanho, 0)) <= 0) {
        printf("Problemas com a conexão\n");
        exit(0);
    }

    b[n] = '\0';
    return n;
}

long tamanhoArquivoByte(char *caminho) {
    FILE *file;
    long size;
    file = fopen(caminho, "rb");
    if (file) {
        fseek(file, 0, SEEK_END);
        size = ftell(file);
        rewind(file);
        fclose(file);
        return size;
    } else {
        return 0;
    }
}

unsigned checksum(char *c) {
    FILE *f;
    unsigned char *buf, *bufI;
    unsigned int seed = 0;
    long size;
    size_t i, result;

    if (NULL == (f = fopen(c, "rb"))) {
        return 0;
    }

    size = tamanhoArquivoByte(c);
    buf = (unsigned char *) malloc(sizeof(unsigned char) * size);
    bufI = buf;

    if (buf == NULL) {
        return 0;
    }

    result = fread(buf, 1, size, f);
    if (result != size) {
        free(buf);
        fclose(f);
        return 0;
    }

    for (i = 0; i < result; ++i) {
        seed += (unsigned int) (*buf++);
    }

    fclose(f);
    free(bufI);
    return seed;
}

int enviarArquivoTcp(int connfd, char *c) {
    FILE *f;
    struct CabecaoArquivo cArquivo;

    char buffer[BUFFSIZEFILE];
    long size;
    int check, qtdParteInteira = 0, n, i;

    size = tamanhoArquivoByte(c);
    check = checksum(c);

    if (NULL == (f = fopen(c, "rb")) || size <= 0 || check <= 0) {
        printf("\nNão foi possível enviar esse arquivo.\n\n");
        cArquivo.arquivo[0] = '\0';
        strcat(cArquivo.arquivo, "erro");
        send(connfd, &cArquivo, sizeof (struct CabecaoArquivo), 0);
        return 1;
    }

    qtdParteInteira = size / BUFFSIZEFILE;
    if (size % BUFFSIZEFILE != 0) {
        qtdParteInteira++;
    }

    cArquivo.arquivo[0] = '\0';
    strcat(cArquivo.arquivo, c);
    cArquivo.size = size;
    cArquivo.sum = check;
    cArquivo.partes = qtdParteInteira;

    send(connfd, &cArquivo, sizeof (struct CabecaoArquivo), 0);

    memset(buffer, 0, sizeof(buffer));

    printf("Enviando arquivo ...\n");
    for (i = 0; i < qtdParteInteira; i++) {
        n = fread(buffer, 1, BUFFSIZEFILE, f);
        send(connfd, buffer, n, 0);
    }

    fclose(f);
    return 0;
}