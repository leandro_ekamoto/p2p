#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <dirent.h>

#define MAXRECVSTRING 255
#define PORTA 55555
#define BUFFSIZE 200
#define SA struct sockaddr
#define MAXPENDING 1000
#define BUFFSIZEFILE 512
#define BUFFSIZE2 800

/* Variável que controla quando mostra o menu*/
int mostra_menu = 1;

/* Interface da placa de rede */
char interface[100];

typedef enum { false, true } bool;

/* Estrutura utilizada para transferir informações entre aplicações */
struct Param{
  int opc;
  char data[5000];
  char ip_origem[100];
};

/* Estrutura para transferir arquivo*/
struct CabecalhoArquivo {
    char arquivo[250];
    long size;
    unsigned sum;
    int partes;
};

/* Retorna o tamanho do arquivo */
long tamanhoArquivoByte(char *caminho) {
    FILE *file;
    long size;
    file = fopen(caminho, "rb");
    if (file) {
        fseek(file, 0, SEEK_END);
        size = ftell(file);
        rewind(file);
        fclose(file);
        return size;
    } else {
        return 0;
    }
}

/* Busca IP da máquina que está executando o programa */
char * getIP() {

    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name , interface , IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &ifr);

    close(fd);

    return inet_ntoa(( (struct sockaddr_in *)&ifr.ifr_addr )->sin_addr);
}

/* Responder broadcast que solicita usuários conectados */
void responderUsuarios(char ip[100]) {

    int sockfd, received = 0, bytes = 0;
    struct sockaddr_in servaddr;
    char buffer[BUFFSIZE];
    int opcao;
    unsigned int echolen;
    struct Param data;
    int i = 0;
    char *ip_origem = getIP();
    int tamanho_vetor = strlen(ip_origem);

    for(i = 0 ; i < tamanho_vetor ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }
    data.ip_origem[tamanho_vetor] = '\0';

    data.opc = 1;

    if((sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    printf("Falha ao criar o socket");

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 60000;
    timeout.tv_usec = 0;

    //Time out Envio
    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("setsockopt failed\n");

    //Time out Recebimento
    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("setsockopt failed\n");
    ////////////////////////////////////////////////////////////////

    /* Inicializando a estrutura */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip);
    servaddr.sin_port = htons(PORTA);

    /* Estabelecendo conexao */
    if (connect(sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
        printf("Falha ao conectar ao servidor");

    //echolen = strlen(ip);
    if (send(sockfd, (void*)&data, sizeof(data), 0) != sizeof(data))
        printf("Envio falhou ao enviar todos os bytes");

    close(sockfd);

}

/* Responder broadcast que solicita lista de arquivos dos usuários conectados */
void responderListaDeArquivos(char ip[100]) {

    int sockfd, received = 0, bytes = 0;
    struct sockaddr_in servaddr;
    char buffer[BUFFSIZE];
    int opcao;
    unsigned int echolen;
    struct Param data;
    int i = 0;
    char *ip_origem = getIP();
    int tamanho_vetor = strlen(ip_origem);

    for(i = 0 ; i < tamanho_vetor ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }
    data.ip_origem[tamanho_vetor] = '\0';

    data.opc = 2;

    if((sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
      printf("Falha ao criar o socket");

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 60000;
    timeout.tv_usec = 0;

    //Time out Envio
    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("setsockopt failed\n");

    //Time out Recebimento
    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("setsockopt failed\n");
    ////////////////////////////////////////////////////////////////

    /* Inicializando a estrutura */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip);
    servaddr.sin_port = htons(PORTA);

    /* Estabelecendo conexao */
    if (connect(sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
        printf("Falha ao conectar ao servidor");

    DIR *d;
    struct dirent *dir;
    d = opendir("./arquivos");
    if (d) {

        sprintf(data.data, "%s", "\0");
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG){
                char aux[1000];
                sprintf(aux, "%s %s\n", data.ip_origem, dir->d_name);
                strcat(data.data, aux);
            }
        }
    }
    closedir(d);

    if (send(sockfd, (void*)&data, sizeof(data), 0) != sizeof(data))
        printf("Envio falhou ao enviar todos os bytes");

    close(sockfd);
}

/* Responder broadcast que busca arquivo em todos os usuários conectados */
void responderBuscaArquivo(char ip[100], char nome_arquivo[100]) {

    int sockfd, received = 0, bytes = 0;
    struct sockaddr_in servaddr;
    char buffer[BUFFSIZE];
    int opcao;
    unsigned int echolen;
    struct Param data;
    int i = 0;
    char *ip_origem = getIP();
    int tamanho_vetor = strlen(ip_origem);

    for(i = 0 ; i < tamanho_vetor ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }
    data.ip_origem[tamanho_vetor] = '\0';
    data.opc = 3;

    if((sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    printf("Falha ao criar o socket");

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 60000;
    timeout.tv_usec = 0;

    //Time out Envio
    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("setsockopt failed\n");

    //Time out Recebimento
    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("setsockopt failed\n");
    ////////////////////////////////////////////////////////////////

    /* Inicializando a estrutura */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip);
    servaddr.sin_port = htons(PORTA);

    /* Estabelecendo conexao */
    if (connect(sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
        printf("Falha ao conectar ao servidor");

    DIR *d;
    struct dirent *dir;
    d = opendir("./arquivos");
    if (d) {

        sprintf(data.data, "%s\n", "F");

        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG){
                if (strcmp (nome_arquivo, dir->d_name) == 0) {

                    sprintf(data.data, "%s\n", "V");
                    break;
                }
            }
        }
    }
    closedir(d);

    if (send(sockfd, (void*)&data, sizeof(data), 0) != sizeof(data))
        printf("Envio falhou ao enviar todos os bytes");

    close(sockfd);
}

/* Salva log de todos as operações realizadas como servidor */
void escreverLog(char ip[100], char *opc) {

  char nome_arquivo[100];
  sprintf(nome_arquivo, "log/log_%s", __DATE__);

	FILE *arq;

	arq = fopen(nome_arquivo, "a");
	if(arq == NULL)
        printf("Erro, nao foi possivel abrir o arquivo\n");
	else {

		fprintf(arq, "%s %s (%s) - %s\n", __DATE__, __TIME__, ip, opc);
	}

	fclose(arq);
}

/* Envia arquivo para usuário que o solicitou */
void enviarArquivoTcp(int connfd, char *c) {

    FILE *f;
    struct CabecalhoArquivo cArquivo;
    char buffer[BUFFSIZEFILE];
    long size;
    int qtdParteInteira = 0, n, i;
    size = tamanhoArquivoByte(c);

    if (NULL == (f = fopen(c, "rb")) || size <= 0 ) {

        cArquivo.arquivo[0] = '\0';
        strcat(cArquivo.arquivo, "erro");
        send(connfd, &cArquivo, sizeof (struct CabecalhoArquivo), 0);
        return;
    }

    qtdParteInteira = size / BUFFSIZEFILE;

    if (size % BUFFSIZEFILE != 0) {
        qtdParteInteira++;
    }

    cArquivo.arquivo[0] = '\0';
    strcat(cArquivo.arquivo, c);
    cArquivo.size = size;

    cArquivo.partes = qtdParteInteira;

    send(connfd, &cArquivo, sizeof (struct CabecalhoArquivo), 0);

    memset(buffer, 0, sizeof(buffer));
    for (i = 0; i < qtdParteInteira; i++) {

        n = fread(buffer, 1, BUFFSIZEFILE, f);
        send(connfd, buffer, n, 0);
    }

    sleep(1);
    close(connfd);
    fclose(f);
}

/* Recebe conexões direcionadas ao seu IP */
void *threadReceberNoIP(void *ip) {

    int listenfd, connfd, clientlen, n, tamanho_resposta, bytes=0, received=0;
    char buffer[BUFFSIZE];
    char resposta[BUFFSIZE];
    struct sockaddr_in servaddr, client;
    struct Param data;

    if((listenfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    printf("Falha ao criar o socket");

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip);

    servaddr.sin_port = htons(PORTA);

    if(bind(listenfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
    printf("Falha ao observar o socket do servidor");

    if(listen(listenfd, MAXPENDING) < 0)
    printf("Falha ao tentar escutar o socket do servidor");

    for( ; ; ) {

      n = -1;
      clientlen = sizeof(client);

      if((connfd = accept(listenfd, (SA *) &client, &clientlen)) < 0)
        printf("Falhou ao aceitar a conexao do cliente");

      if((bytes = recv(connfd, (void*)&data, sizeof(data), 0)) < 0)
        printf("Falhou ao receber os dados iniciais do cliente");

      sleep(1);
      char *ip_origem_aux = data.ip_origem;

      switch (data.opc) {
        case 1:

          printf("%s\n", ip_origem_aux);

          // Mostra o menu
          mostra_menu = 1;
        break;

        case 2:

          printf("%s\n",data.data);
          mostra_menu = 1;
        break;

        case 3:

          printf("%s %s\n",data.ip_origem ,data.data);
          mostra_menu = 1;
        break;

        case 4:

          enviarArquivoTcp(connfd, data.data);
        break;

        default :
          printf ("Valor invalido!\n");
      }

      close(connfd);
    }
    exit(0);
}

/* Recebe dados por broadcast */
void *threadRecebimentoBroadcast()
{

    int sock;
    struct sockaddr_in broadcastAddr;
    unsigned short broadcastPort;
    char recvString[MAXRECVSTRING+1];
    int recvStringLen;
    struct Param data;

    broadcastPort = PORTA;

    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        printf("socket() failed");

    memset(&broadcastAddr, 0, sizeof(broadcastAddr));
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    broadcastAddr.sin_port = htons(broadcastPort);

    if (bind(sock, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) < 0)
        printf("bind() failed");

    /*Entra em lupe para enviar e receber mensagens*/
    for (;;) {

        if ((recvStringLen = recvfrom(sock, (void*)&data, sizeof(data), 0, NULL, 0)) < 0)
            printf("recvfrom() failed");

        mostra_menu = 0;
        switch (data.opc) {
             case 1:

                escreverLog(data.ip_origem, "Solicitar (via BROADCAST 1 ) a lista de usuários da RCA");
                responderUsuarios(data.ip_origem);
             break;

             case 2:

                escreverLog(data.ip_origem, "Solicitar lista de arquivos de todos os programas");
                responderListaDeArquivos(data.ip_origem);
             break;

             case 3:

                escreverLog(data.ip_origem, "Procurar um arquivo específico");
                responderBuscaArquivo(data.ip_origem, data.data);
             break;

             case 4:

                // Baixar arquivo não será por broadcast
             break;
             default :
               printf ("Valor invalido!\n");
       }

    }

    close(sock);
    exit(0);
}

/* Enviar broadcast solicitando os usuários conectados */
void solicitarUsuarios() {

    int sockfd;
    struct sockaddr_in their_addr;
    struct hostent *he;
    int numbytes;
    int broadcast = 1;
    char endereco_broadcast[] = "255.255.255.255";
    struct Param data;
    char * ip_origem = getIP();
    int i = 0;
    int quantidade_caracteres = strlen(ip_origem);

    for(i = 0 ; i < quantidade_caracteres ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }
    data.ip_origem[quantidade_caracteres] = '\0';

    data.opc = 1;

    if ((he = gethostbyname(endereco_broadcast)) == NULL) {
        printf("gethostbyname");
        exit(1);
    }

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        printf("socket");
        exit(1);
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast,
        sizeof broadcast) == -1) {
        printf("setsockopt (SO_BROADCAST)");
        exit(1);
    }

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 60000;
    timeout.tv_usec = 0;

    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("Falha ao setar timeout recebimento\n");

    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("Falha ao setar timeout envio\n");
    ////////////////////////////////////////////////////////////////

    their_addr.sin_family = AF_INET;
    their_addr.sin_port = htons(PORTA);
    their_addr.sin_addr = *((struct in_addr *)he->h_addr);
    memset(their_addr.sin_zero, '\0', sizeof their_addr.sin_zero);

    if ((numbytes = sendto(sockfd, (void*)&data, sizeof(data), 0,(struct sockaddr *)&their_addr, sizeof their_addr)) == -1) {
        printf("Falha ao enviar");
        exit(1);
    }

    close(sockfd);

    printf("\nAguardando retorno...\n");
    sleep(3);
}

/* Enviar broadcast passando a operação */
void enviarBroadcast(int operacao) {

    int sockfd;
    struct sockaddr_in their_addr;
    struct hostent *he;
    int numbytes;
    int broadcast = 1;
    char endereco_broadcast[] = "255.255.255.255";
    struct Param data;
    char * ip_origem = getIP();
    int i = 0;
    int quantidade_caracteres = strlen(ip_origem);

    for(i = 0 ; i < quantidade_caracteres ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }
    data.ip_origem[quantidade_caracteres] = '\0';
    data.opc = operacao;

    if ((he = gethostbyname(endereco_broadcast)) == NULL) {
        printf("Falha na função gethostbyname");
        exit(1);
    }

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        printf("Falha ao criar socket");
        exit(1);
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast,sizeof broadcast) == -1) {
        printf("Falha ao setsockopt (SO_BROADCAST)");
        exit(1);
    }

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 60000;
    timeout.tv_usec = 0;

    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("Falha ao setsockopt failed\n");

    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("Falha ao setsockopt failed\n");
    ////////////////////////////////////////////////////////////////

    their_addr.sin_family = AF_INET;
    their_addr.sin_port = htons(PORTA);
    their_addr.sin_addr = *((struct in_addr *)he->h_addr);
    memset(their_addr.sin_zero, '\0', sizeof their_addr.sin_zero);

    if ((numbytes = sendto(sockfd, (void*)&data, sizeof(data), 0,(struct sockaddr *)&their_addr, sizeof their_addr)) == -1) {
        printf("Falha ao sendto");
        exit(1);
    }

    close(sockfd);

    printf("\nAguardando retorno...\n");
    sleep(3);
}

/* Enviar broadcast solicitando os arquivos dos usuários conectados */
void buscarArquivoBroadcast() {

    int sockfd;
    struct sockaddr_in their_addr;
    struct hostent *he;
    int numbytes;
    int broadcast = 1;
    char endereco_broadcast[] = "255.255.255.255";
    struct Param data;
    char * ip_origem = getIP();
    int i = 0;
    int quantidade_caracteres = strlen(ip_origem);

    for(i = 0 ; i < quantidade_caracteres ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }
    data.ip_origem[quantidade_caracteres] = '\0';

    printf("Digite o nome do arquivo:");
    scanf("%s", data.data);
    while(getchar() != '\n');

    data.opc = 3;

    if ((he=gethostbyname(endereco_broadcast)) == NULL) {
        printf("Falha ao gethostbyname");
        exit(1);
    }

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        printf("Falha ao criar socket");
        exit(1);
    }

    if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast,
        sizeof broadcast) == -1) {
        printf("Falha ao setsockopt (SO_BROADCAST)");
        exit(1);
    }

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 60000;
    timeout.tv_usec = 0;

    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("Falha ao setsockopt failed\n");

    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        printf("Falha ao failed\n");
    ////////////////////////////////////////////////////////////////

    their_addr.sin_family = AF_INET;
    their_addr.sin_port = htons(PORTA);
    their_addr.sin_addr = *((struct in_addr *)he->h_addr);
    memset(their_addr.sin_zero, '\0', sizeof their_addr.sin_zero);

    if ((numbytes = sendto(sockfd, (void*)&data, sizeof(data), 0,(struct sockaddr *)&their_addr, sizeof their_addr)) == -1) {
        printf("Falha ao sendto");
        exit(1);
    }

    close(sockfd);

    printf("\nAguardando retorno...\n");
    sleep(3);
}

/* Executa download de um arquivo pelo ip da máquina que possui o arquivo */
void downloadArquivo() {

    int sockfd, received = 0, bytes = 0;
    struct sockaddr_in servaddr;
    char buffer[BUFFSIZE];
    int opcao, n;
    unsigned int echolen;
    struct Param data;
    struct CabecalhoArquivo cArquivo;
    char buffer2[BUFFSIZE2], bufferA[BUFFSIZEFILE], lixo;
    int i = 0;
    char *ip_origem = getIP();
    int tamanho_vetor = strlen(ip_origem);
    char mensagem_log[200];

    for(i = 0 ; i < tamanho_vetor ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }

    data.ip_origem[tamanho_vetor] = '\0';
    data.opc = 4;

    printf("IP da máquina:");
    char ip_maquina[100];
    scanf("%s", ip_maquina);
    while(getchar() != '\n');

    char nome_arq[100];
    printf("Digite o nome do arquivo:");
    scanf("%s", nome_arq);
    while(getchar() != '\n');

    sprintf(data.data, "arquivos/%s", nome_arq);

    if((sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    printf("Falha ao criar o socket");

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip_maquina);
    servaddr.sin_port = htons(PORTA);

    /* Estabelecendo conexao */
    if (connect(sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
        printf("Falha ao conectar ao servidor");

    /* Solicita o Arquivo */
    if (send(sockfd, (void*)&data, sizeof(data), 0) != sizeof(data))
        printf("Envio falhou ao enviar todos os bytes");

    /* Recebimento de arquivo */
    if ((n = recv(sockfd, &cArquivo, sizeof (struct CabecalhoArquivo), 0)) <= 0) {
        printf("Problemas com a conexão\n");
        exit(0);
    }

    if (strcmp(cArquivo.arquivo, "erro") == 0) {
        printf("Problemas ao tentar fazer download do arquivo.\n");
    } else {

        int j = 0;
        //FILE *fn = fopen(cArquivo.arquivo, "wb");
        FILE *fn = fopen("arquivos/shindi.mp4", "wb");

        printf("Efetuando Download ...\n");

        while (cArquivo.partes != j) {

            j++;

            if ((n = recv(sockfd, bufferA, BUFFSIZEFILE, 0)) <= 0) {

                printf("Problemas com a conexão\n");
                exit(0);
            }

            if (cArquivo.partes != j || cArquivo.size % BUFFSIZEFILE == 0) {

                fwrite(bufferA, 1, BUFFSIZEFILE, fn);
            } else {

                fwrite(bufferA, 1, (cArquivo.size % BUFFSIZEFILE), fn);
            }
        }

        sleep(1);
        close(sockfd);
        fclose(fn);

        printf("Download feito com sucesso.\n\n");

        sprintf(mensagem_log, "Download do arquivo: %s", cArquivo.arquivo);

        escreverLog(data.ip_origem, mensagem_log);

    }
}

/* Menu com as opções que o usuário poderá executar */
void threadMenu()
{
    int opc;

    while(1)
    {
        if(!mostra_menu)
        {
            continue;
        }

        opc = 0;
        printf("\nEntre com uma opção para a RCA:\n");
        printf("1 - Solicitar lista de usuários\n");
        printf("2 - Solicitar lista de arquivos de todos os programas\n");
        printf("3 - Procurar um arquivo específico\n");
        printf("4 - Baixar arquivo de um determinado servidor\n");
        printf("0 - Sair\n");
        printf("->");
        scanf("%i",&opc);

        if(opc == 0) {
            break;
        }

        switch (opc) {
             case 1:
                solicitarUsuarios();
             break;

             case 2:
                enviarBroadcast(2);
             break;

             case 3:
                buscarArquivoBroadcast();
             break;

             case 4:
                downloadArquivo();
             break;
             default :
               printf ("Valor invalido!\n");
       }

    }
}

int main(int argc, char *argv[]) {

    printf("Interface de rede utilizada:");
    scanf("%s", interface);
    while(getchar() != '\n');

    pthread_t pthRecebimentoBroadcast, pthMenu, pthRecebeNoIp;
    pthread_create(&pthRecebimentoBroadcast,NULL,threadRecebimentoBroadcast, &argv);
    pthread_create(&pthRecebeNoIp,NULL,threadReceberNoIP, (void *)getIP());
    threadMenu();

    exit(0);
}
