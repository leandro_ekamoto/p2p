#include <stdio.h>          /* printf */
#include <stdlib.h>         /* exit */
#include <string.h>         /* bzero */
#include <sys/socket.h>     /* struct sockaddr, socket */
#include <netinet/in.h>     /* struct sockaddr_in */
#include <arpa/inet.h>      /* inet_pton, htons */
#include <unistd.h>         /* read */
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <dirent.h>

#define MAXRECVSTRING 255
#define PORTA 55555
#define BUFFSIZE 200
#define SA struct sockaddr
#define MAXPENDING 5
int mostra_menu = 1;

typedef enum { false, true } bool;


struct Param{
    int opc;
    char *ip;
    char ip_origem[100];
    char data[100];
};

struct Param2{
    int opc;
    char data[5000];
    char ip_origem[100];
};

char * getIP() {

    int fd;
    struct ifreq ifr;

    // MUITO IMPORTANTE VERIFICAR A INTERFACE DE REDE
    // enp0s3
    char iface[] = "wlp2s0";

    fd = socket(AF_INET, SOCK_DGRAM, 0);

    //Type of address to retrieve - IPv4 IP address
    ifr.ifr_addr.sa_family = AF_INET;

    //Copy the interface name in the ifreq structure
    strncpy(ifr.ifr_name , iface , IFNAMSIZ-1);

    ioctl(fd, SIOCGIFADDR, &ifr);

    close(fd);

    return inet_ntoa(( (struct sockaddr_in *)&ifr.ifr_addr )->sin_addr);
}

void responderUsuarios(char ip[100]) {
    // TODO:leandro
    //printf("Respondendo IP: %s\n\n", ip);

    int sockfd, received = 0, bytes = 0;
    struct sockaddr_in servaddr;
    char buffer[BUFFSIZE];
    int opcao;
    unsigned int echolen;
    struct Param2 data;

    int i = 0;

    char *ip_origem = getIP();
    int tamanho_vetor = strlen(ip_origem);

    for(i = 0 ; i < tamanho_vetor ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }
    data.ip_origem[tamanho_vetor] = '\0';

    data.opc = 1;

    if((sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    printf("Falha ao criar o socket");

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 1000;
    timeout.tv_usec = 0;

    //Time out Envio
    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");

    //Time out Recebimento
    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");
    ////////////////////////////////////////////////////////////////

    /* Inicializando a estrutura */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip);
    servaddr.sin_port = htons(PORTA);

    /* Estabelecendo conexao */
    if (connect(sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
        printf("Falha ao conectar ao servidor");

    //echolen = strlen(ip);
    if (send(sockfd, (void*)&data, sizeof(data), 0) != sizeof(data))
        printf("Envio falhou ao enviar todos os bytes");

    close(sockfd);
}

void responderListaDeArquivos(char ip[100]) {

    int sockfd, received = 0, bytes = 0;
    struct sockaddr_in servaddr;
    char buffer[BUFFSIZE];
    int opcao;
    unsigned int echolen;
    struct Param2 data;

    int i = 0;

    char *ip_origem = getIP();
    int tamanho_vetor = strlen(ip_origem);

    for(i = 0 ; i < tamanho_vetor ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }
    data.ip_origem[tamanho_vetor] = '\0';

    data.opc = 2;

    if((sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    printf("Falha ao criar o socket");

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 1000;
    timeout.tv_usec = 0;

    //Time out Envio
    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");

    //Time out Recebimento
    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");
    ////////////////////////////////////////////////////////////////

    /* Inicializando a estrutura */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip);
    servaddr.sin_port = htons(PORTA);

    /* Estabelecendo conexao */
    if (connect(sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
        printf("Falha ao conectar ao servidor");

    
    DIR *d;
    struct dirent *dir;
    d = opendir("./arquivos");
    if (d) {

        sprintf(data.data, "%s", "\0");
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG){
                char aux[1000];
                sprintf(aux, "%s %s\n", data.ip_origem, dir->d_name);
                strcat(data.data, aux);
            }
        }
    }
    closedir(d);

    //echolen = strlen(ip);
    if (send(sockfd, (void*)&data, sizeof(data), 0) != sizeof(data))
        printf("Envio falhou ao enviar todos os bytes");

    close(sockfd);
}

void responderBuscaArquivo(char ip[100], char nome_arquivo[100]) {

    int sockfd, received = 0, bytes = 0;
    struct sockaddr_in servaddr;
    char buffer[BUFFSIZE];
    int opcao;
    unsigned int echolen;
    struct Param2 data;

    int i = 0;

    char *ip_origem = getIP();
    int tamanho_vetor = strlen(ip_origem);

    for(i = 0 ; i < tamanho_vetor ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }
    data.ip_origem[tamanho_vetor] = '\0';

    data.opc = 3;

    if((sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    printf("Falha ao criar o socket");

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 1000;
    timeout.tv_usec = 0;

    //Time out Envio
    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");

    //Time out Recebimento
    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");
    ////////////////////////////////////////////////////////////////

    /* Inicializando a estrutura */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip);
    servaddr.sin_port = htons(PORTA);

    /* Estabelecendo conexao */
    if (connect(sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
        printf("Falha ao conectar ao servidor");

    
    DIR *d;
    struct dirent *dir;
    d = opendir("./arquivos");
    if (d) {

        sprintf(data.data, "%s\n", "F");

        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG){
                if (strcmp (nome_arquivo, dir->d_name) == 0) {
                    sprintf(data.data, "%s\n", "V");
                    break;
                }
            }
        }
    }
    closedir(d);

    //echolen = strlen(ip);
    if (send(sockfd, (void*)&data, sizeof(data), 0) != sizeof(data))
        printf("Envio falhou ao enviar todos os bytes");

    close(sockfd);
}

void escreverLog(char ip[100], char *opc) {

    char nome_arquivo[100];
    sprintf(nome_arquivo, "log/log_%s", __DATE__);

	FILE *arq;

	arq = fopen(nome_arquivo, "a");
	if(arq == NULL)
        printf("Erro, nao foi possivel abrir o arquivo\n");
	else {

		fprintf(arq, "%s %s (%s) - %s\n", __DATE__, __TIME__, ip, opc);
	}

	fclose(arq);
}

// Recebe tudo direcionado ao seu IP
void *threadReceberNoIP(void *ip) {

    int listenfd, connfd, clientlen, n, tamanho_resposta, bytes=0, received=0;
    char buffer[BUFFSIZE];
    char resposta[BUFFSIZE];
    struct sockaddr_in servaddr, client;
    struct Param2 data;

    if((listenfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    printf("Falha ao criar o socket");

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    //servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_addr.s_addr = inet_addr(ip);

    servaddr.sin_port = htons(PORTA);

    if(bind(listenfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
    printf("Falha ao observar o socket do servidor");

    if(listen(listenfd, MAXPENDING) < 0)
    printf("Falha ao tentar escutar o socket do servidor");

    for( ; ; ) {

      n = -1;
      clientlen = sizeof(client);

      if((connfd = accept(listenfd, (SA *) &client, &clientlen)) < 0)
	    printf("Falhou ao aceitar a conexao do cliente");

      //Imprime o IP e porta do cliente
      //printf("Cliente conectado: %s:%d\n", inet_ntoa(client.sin_addr),client.sin_port);

      if((bytes = recv(connfd, (void*)&data, sizeof(data), 0)) < 0)
	    printf("Falhou ao receber os dados iniciais do cliente");

        switch (data.opc) {
            case 1:
                printf("%s\n", data.ip_origem);

                // Mostra o menu
                mostra_menu = 1;
                break;

            case 2:
                printf("%s\n",data.data);
                mostra_menu = 1;
                break;

            case 3:
                printf("%s %s\n",data.ip_origem ,data.data);
                mostra_menu = 1;
                break;

            case 4:
                printf("Baixar arquivo de um determinado servidor\n");
                break;

            default :
                printf ("Valor invalido!\n");
        }

      close(connfd);
    }
    exit(0);
}

void *threadRecebimentoBroadcast()
{

    int sock;                         /* Socket */
    struct sockaddr_in broadcastAddr; /* Broadcast Address */
    unsigned short broadcastPort;     /* Port */
    char recvString[MAXRECVSTRING+1]; /* Buffer for received string */
    int recvStringLen;                /* Length of received string */
    struct Param data;

    broadcastPort = PORTA;   /* First arg: broadcast port */

    /* Create a best-effort datagram socket using UDP */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        printf("socket() failed");


    /* Construct bind structure */
    memset(&broadcastAddr, 0, sizeof(broadcastAddr));   /* Zero out structure */
    broadcastAddr.sin_family = AF_INET;                 /* Internet address family */
    broadcastAddr.sin_addr.s_addr = htonl(INADDR_ANY);  /* Any incoming interface */
    broadcastAddr.sin_port = htons(broadcastPort);      /* Broadcast port */

    /* Bind to the broadcast port */
    if (bind(sock, (struct sockaddr *) &broadcastAddr, sizeof(broadcastAddr)) < 0)
        printf("bind() failed");

    /*Entra em lupe para enviar e receber mensagens*/
    for (;;) {

        /* Receive a single datagram from the server */
        if ((recvStringLen = recvfrom(sock, (void*)&data, sizeof(data), 0, NULL, 0)) < 0)
            printf("recvfrom() failed");

        // TODO: Dependendo do que ele receber irá executar uma coisa:
        //
        // 1. Armazenar o log das solicitações que foram feitas com: IP, tipo de
        // solicitação, data e hora (não é necessário mostrar na tela, apenas
        // armazene);
        // 2. Responder consulta sobre seu IP (“usuário” de rede)
        // 3. Fornecer a lista de arquivos contidos em seu diretório;
        // 4. Responder positivamente caso possua determinado arquivo;
        // 5. Enviar um determinado arquivo solicitado por outro usuário,
        // cronometrar o tempo gasto para a transferência e o salvar no log.
        //
        mostra_menu = 0;
        switch (data.opc) {
             case 1:
                escreverLog(data.ip_origem, "Solicitar (via BROADCAST 1 ) a lista de usuários da RCA");
                responderUsuarios(data.ip_origem);

             break;

             case 2:
                escreverLog(data.ip_origem, "Solicitar lista de arquivos de todos os programas");
                responderListaDeArquivos(data.ip_origem);
             break;

             case 3:
                escreverLog(data.ip_origem, "Procurar um arquivo específico");
                responderBuscaArquivo(data.ip_origem, data.data);
             break;

             case 4:
                printf("Baixar arquivo de um determinado servidor\n");
             break;
             default :
               printf ("Valor invalido!\n");
       }

    }

    close(sock);
    exit(0);
}



void solicitarUsuarios() {

    int sockfd;
    struct sockaddr_in their_addr; // connector's address information
    struct hostent *he;
    int numbytes;
    int broadcast = 1;
    char endereco_broadcast[] = "255.255.255.255";
    struct Param data;

    char * ip_origem = getIP();

    int i = 0;
    int quantidade_caracteres = strlen(ip_origem);

    //printf("quantidade de caracteres:%i\n",quantidade_caracteres);

    for(i = 0 ; i < quantidade_caracteres ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }

    //printf("%s", data.nome);

    //sprintf(data.nome, "%s", "LEANDRO\0");

    data.opc = 1;

    if ((he=gethostbyname(endereco_broadcast)) == NULL) {  // get the host info
        perror("gethostbyname");
        exit(1);
    }

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    // this call is what allows broadcast packets to be sent:
    if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast,
        sizeof broadcast) == -1) {
        perror("setsockopt (SO_BROADCAST)");
        exit(1);
    }

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 1000;
    timeout.tv_usec = 0;

    //Time out Envio
    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");

    //Time out Recebimento
    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");
    ////////////////////////////////////////////////////////////////

    their_addr.sin_family = AF_INET;     // host byte order
    their_addr.sin_port = htons(PORTA); // short, network byte order
    their_addr.sin_addr = *((struct in_addr *)he->h_addr);
    memset(their_addr.sin_zero, '\0', sizeof their_addr.sin_zero);

    if ((numbytes = sendto(sockfd, (void*)&data, sizeof(data), 0,
             (struct sockaddr *)&their_addr, sizeof their_addr)) == -1) {
        perror("sendto");
        exit(1);
    }

    close(sockfd);

    printf("\nAguardando retorno...\n");
    sleep(3);
}

void enviarBroadcast(int operacao) {

    int sockfd;
    struct sockaddr_in their_addr; // connector's address information
    struct hostent *he;
    int numbytes;
    int broadcast = 1;
    char endereco_broadcast[] = "255.255.255.255";
    struct Param data;

    char * ip_origem = getIP();

    int i = 0;
    int quantidade_caracteres = strlen(ip_origem);

    //printf("quantidade de caracteres:%i\n",quantidade_caracteres);

    for(i = 0 ; i < quantidade_caracteres ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }

    //printf("%s", data.nome);

    //sprintf(data.nome, "%s", "LEANDRO\0");

    data.opc = operacao;

    if ((he=gethostbyname(endereco_broadcast)) == NULL) {  // get the host info
        perror("gethostbyname");
        exit(1);
    }

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    // this call is what allows broadcast packets to be sent:
    if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast,
        sizeof broadcast) == -1) {
        perror("setsockopt (SO_BROADCAST)");
        exit(1);
    }

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 1000;
    timeout.tv_usec = 0;

    //Time out Envio
    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");

    //Time out Recebimento
    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");
    ////////////////////////////////////////////////////////////////

    their_addr.sin_family = AF_INET;     // host byte order
    their_addr.sin_port = htons(PORTA); // short, network byte order
    their_addr.sin_addr = *((struct in_addr *)he->h_addr);
    memset(their_addr.sin_zero, '\0', sizeof their_addr.sin_zero);

    if ((numbytes = sendto(sockfd, (void*)&data, sizeof(data), 0,
             (struct sockaddr *)&their_addr, sizeof their_addr)) == -1) {
        perror("sendto");
        exit(1);
    }

    close(sockfd);

    printf("\nAguardando retorno...\n");
    sleep(3);
}

void buscarArquivoBroadcast() {

    int sockfd;
    struct sockaddr_in their_addr; // connector's address information
    struct hostent *he;
    int numbytes;
    int broadcast = 1;
    char endereco_broadcast[] = "255.255.255.255";
    struct Param data;

    char * ip_origem = getIP();

    int i = 0;
    int quantidade_caracteres = strlen(ip_origem);

    //printf("quantidade de caracteres:%i\n",quantidade_caracteres);

    for(i = 0 ; i < quantidade_caracteres ; i++)
    {
        data.ip_origem[i] = ip_origem[i];
    }

    printf("Digite o nome do arquivo:");
    scanf("%s", data.data);
    while(getchar() != '\n');

    //printf("%s", data.nome);

    //sprintf(data.nome, "%s", "LEANDRO\0");

    data.opc = 3;

    if ((he=gethostbyname(endereco_broadcast)) == NULL) {  // get the host info
        perror("gethostbyname");
        exit(1);
    }

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }

    // this call is what allows broadcast packets to be sent:
    if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast,
        sizeof broadcast) == -1) {
        perror("setsockopt (SO_BROADCAST)");
        exit(1);
    }

    ////////////////////////////////TIMEOUT////////////////////////////////
    struct timeval timeout;
    timeout.tv_sec = 1000;
    timeout.tv_usec = 0;

    //Time out Envio
    if (setsockopt (sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");

    //Time out Recebimento
    if (setsockopt (sockfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0)
        perror("setsockopt failed\n");
    ////////////////////////////////////////////////////////////////

    their_addr.sin_family = AF_INET;     // host byte order
    their_addr.sin_port = htons(PORTA); // short, network byte order
    their_addr.sin_addr = *((struct in_addr *)he->h_addr);
    memset(their_addr.sin_zero, '\0', sizeof their_addr.sin_zero);

    if ((numbytes = sendto(sockfd, (void*)&data, sizeof(data), 0,
             (struct sockaddr *)&their_addr, sizeof their_addr)) == -1) {
        perror("sendto");
        exit(1);
    }

    close(sockfd);

    printf("\nAguardando retorno...\n");
    sleep(3);
}


void listarArquivos() {
    DIR *d;
    struct dirent *dir;
    d = opendir("./arquivos");
    printf("Arquivos no diretório: \n");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type == DT_REG){
                printf("%s\n", dir->d_name);
            }
        }
    }
    closedir(d);
}

bool solicitarArquivo(char *fileName) {

    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d){

        while((dir = readdir(d)) != NULL) {

            printf("%s - %s \n", fileName, dir->d_name);

            if (strcmp (fileName, dir->d_name) == 0) {
                printf("ARQUIVO EXISTE\n");
                return true;
            }
        }

    } else {
        perror("Não foi possivel ler seus arquivos");
    }

    printf("ARQUIVO NÃO ENCONTRADO\n");

    return false;
}

void threadMenu()
{
    int opc;

    while(1)
    {
        if(!mostra_menu)
        {
            continue;
        }

        // 1. Solicitar (via BROADCAST 1 ) a lista de usuários da RCA. Devem ser
        // retornados o IP de cada usuário da rede;
        // 2. Solicitar (via BROADCAST) a lista de arquivos de todos os programas
        // executando na rede local;
        // 3. Procurar (via BROADCAST) um arquivo específico na RCA.
        // 4. Baixar um arquivo de um determinado servidor.

        opc = 0;
        printf("\nEntre com uma opção para a RCA:\n");
        printf("1 - Solicitar lista de usuários\n");
        printf("2 - Solicitar lista de arquivos de todos os programas\n");
        printf("3 - Procurar um arquivo específico\n");
        printf("4 - Baixar arquivo de um determinado servidor\n");
        printf("0 - Sair\n");
        printf("->");
        scanf("%i",&opc);

        if(opc == 0) {
            break;
        }

        switch (opc) {
             case 1:
                solicitarUsuarios();
             break;

             case 2:
                enviarBroadcast(2);
             break;

             case 3:
                buscarArquivoBroadcast();
                /*
                printf("Procurar um arquivo específico\n");
                char name[100];
                scanf("%s", name);

                while(getchar() != '\n');

                if (solicitarArquivo(name) == true){
                    printf("ololololololol\n");
                }*/


             break;

             case 4:
                printf("Baixar arquivo de um determinado servidor\n");
             break;
             default :
               printf ("Valor invalido!\n");
       }

    }
}

int main(int argc, char *argv[]) {

    pthread_t pthRecebimentoBroadcast, pthMenu, pthRecebeNoIp;
    pthread_create(&pthRecebimentoBroadcast,NULL,threadRecebimentoBroadcast, &argv);
    pthread_create(&pthRecebeNoIp,NULL,threadReceberNoIP, (void *)getIP());
    threadMenu();

    exit(0);
}


/* Imprime mensagens de erro */
void error(char *msg) {
    printf("%s\n", msg);
    exit(0);
    return;
}
