#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h> 

#define BUFFSIZE 800
#define MAXPENDING 5  
#define SA struct sockaddr
#define SAI struct sockaddr_in

void error(char *msg);
int *criarSocket(int porta);
void inThread(int *listenfd);
static void *execucaoThread(void *arg);
void execucao(int connfd);

int main(int argc, char** argv) {

    int *listenfd, porta;

    porta = atoi(argv[1]);
    listenfd = criarSocket(porta);

    if (listen(*listenfd, MAXPENDING) < 0) {
        error("Falha ao tentar escutar o socket do servidor");
    }

    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            printf("%s\n", dir->d_name);
        }
        closedir(d);
    }

    inThread(listenfd);

    free(listenfd);
    return 0;
}

void error(char *msg) {
    printf("%s\n", msg);
    exit(0);
    return;
}

int *criarSocket(int porta) {
    int *listenfd;
    struct sockaddr_in servaddr;

    listenfd = (int *) malloc(sizeof (int));

    if ((*listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        error("Falha ao criar o socket");
    }

    bzero(&servaddr, sizeof (servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(porta);

    if (bind(*listenfd, (SA *) & servaddr, sizeof (servaddr)) < 0) {
        error("Falha ao observar o socket do servidor");
    }

    return listenfd;
}

void execucao(int connfd) {
    int exit = 0;
    char *menu = "Escolha a opção desejada:\n1)Listar arquivos\n2)Baixar Arquivo via TCP\n3)Baixar Arquivo via UDP\n4)Enviar aquivo\n";

    char buffer[BUFFSIZE];
    int n;

    while (!exit) {
        send(connfd, menu, strlen(menu), 0);

        if ((n = recv(connfd, buffer, BUFFSIZE, 0)) <= 0){
            printf("opção escolhida: %s", buffer);
            return;
        }

        buffer[n] = '\0';
        printf("opção escolhida: %s", buffer);
        if(strcmp(buffer, "1\n") == 0){
            DIR *d;
            struct dirent *dir;
            d = opendir(".");
            char directory[BUFFSIZE];
            if (d) {
                while ((dir = readdir(d)) != NULL) {
                    if (dir->d_type == DT_REG){
                    sprintf(directory, "%s\n", dir->d_name);
                    send(connfd, directory, strlen(directory), 0);
                    }
            }
            closedir(d);
        }
        }
    }
}

static void *execucaoThread(void *arg) {
    int connfd;

    connfd = *((int *) arg);
    pthread_detach(pthread_self());
    execucao(connfd);
    close(connfd);

    return NULL;
}

void inThread(int *listenfd) {
    struct sockaddr_in client;
    socklen_t clientlen;
    int *iptr;
    pthread_t tid;

    for (;;) {
        iptr = (int *) malloc(sizeof (int));
        *iptr = accept(*listenfd, (SA *) & client, &clientlen);
        printf("------------------ Cliente conectado: %s ------------------\n", inet_ntoa(client.sin_addr));

        pthread_create(&tid, NULL, &execucaoThread, iptr);
    }
    return;
}

