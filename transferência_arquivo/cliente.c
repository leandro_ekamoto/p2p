#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define SA struct sockaddr
#define BUFFSIZE 800

/*
 * 
 * 
 */

void error(const char *msg) {
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[]) {
    int sockfd, portno, n, i, n_mensagens, qtd_msg, clientlen, connfd, listenfd;
    struct sockaddr_in serv_addr, client;
    struct hostent *server;

    char buffer[256], lixo;

    if (argc < 3) {
        fprintf(stderr, "usage %s hostname port\n", argv[0]);
        exit(0);
    }
    
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0) {
        error("ERROR opening socket");
    }

    server = gethostbyname(argv[1]);

    if (server == NULL) {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }

    bzero((char *) &serv_addr, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);

    serv_addr.sin_port = htons(portno);

    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        error("Falha ao conectar!");
    }

    while (1) {
        
        if ((n = recv(sockfd, buffer, BUFFSIZE, 0)) <= 0)
            error("Falhou ao receber os dados iniciais do cliente");

        buffer[n] = '\0';
        printf("%s", buffer);
        
        printf("digite a opção: ");
        fgets(buffer, BUFFSIZE, stdin);
        send(sockfd, buffer, strlen(buffer), 0);
    }
    
    return 0;
}
